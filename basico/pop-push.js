/* 
 .pop
 .push
 .shift
 .unshift
*/

var lista = [2, 4, 6, 8, 10];
console.log(lista.length);
console.log(lista[lista.length -1]);

console.log(lista.pop());

var ultimo2 = lista.pop(-1);
console.log(ultimo2);
console.log(lista.pop(-2));

lista.push(18);
console.log(lista.pop());