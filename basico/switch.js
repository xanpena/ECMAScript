// colores/precio: blanco:10, verde:15, azul:20

function priceByColor(color){
    var price;
    switch(color){
        case 'blanco':
            price = 10;
            break;
        case 'verde':
            price = 15;
            break;
        case 'azul':
            price = 20;
            break;
        default:
            price=0;
    }
    return price;
}

priceByColor('blanco');
