document.addEventListener('DOMContentLoaded', launchCookieAdvisor);

function launchCookieAdvisor(){
    var capaCookies = document.createElement('div');
    capaCookies.innerHTML = '<p>Tienes que aceptar las cookies <span id="botonaceptar">Aceptar</span></p>';
    capaCookies.classList.add('cssCookies');
    var todosLosSpanEnDiv = capaCookies.getElementsByTagName('span');
    todosLosSpanEnDiv.item(0).addEventListener('click', function(){
        capaCookies.parentElement.removeChild(capaCookies);
    });
    document.body.appendChild(capaCookies);
}