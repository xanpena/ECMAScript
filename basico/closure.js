function calculadora(op){

    var cortada=20;
    function sumaCortada(val){
        return val + cortada;
    }

}
// Como esta función tira de un valor que está en el scope de la primera función el garbage collector nunca va a eliminar esta referencia
// closures permiten guardar referencias en la anidación de funciones
// ojo con la memoria