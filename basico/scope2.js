var cadena = "Viendo bloques y scope";
console.log(cadena);
{
    var cadena = "Liandola un poco";
    console.log(cadena);
}
console.log(cadena);
// let
let otrostring = "Respeta el scope con let";
console.log(otrostring);
{
    let otrostring = "Que pasa con el scope?";
    console.log(otrostring);
}
console.log(otrostring);