function mapa(x, y, z){
    console.log(x,y,z);
}

mapa(20, 30, 10);

function mapa2(obj){
    console.log(obj.x, obj.y, obj.z);
}

mapa2({x:20, y:30, z:10});

function mapa3(obj){
    for(let key in obj){
        console.log(obj[key]);
    }
    
}

mapa3({x:20, y:30, z:10});
mapa3({x:20, y:30, z:10, t:12});