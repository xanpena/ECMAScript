var cadena = "Hola mundo!";
console.log(typeof(cadena));

var cadena2 = new String("hola");
console.log(typeof(cadena2));

console.log("Tratame como un objeto".length);
console.log(typeof("Tratame como un objeto".length));

var condicion = new Boolean(false);
console.log(typeof(condicion));