// Si el valor pasado por parámetro es mayor que 100 entonces escribe 'Super'
// en otro caso, 'normal'
function decide(dimension){
    if(dimension>100){
        console.log('Super');
    }else{
        console.log('Normal');        
    }
}

decide(10);