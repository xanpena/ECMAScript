const orders = [
    {
      customerName: "Nicolas",
      total: 60,
      delivered: true,
    },
    {
      customerName: "Zulema",
      total: 120,
      delivered: false,
    },
    {
      customerName: "Santiago",
      total: 180,
      delivered: true,
    },
    {
      customerName: "Valentina",
      total: 240,
      delivered: true,
    },
]

const totals = orders.map(item => item.total)

const ordersWithTax = orders.map(item => {
    return {
        ...item,
        tax: .19
    }
})
console.log(ordersWithTax)