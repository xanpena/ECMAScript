function colorear(){
    document.bgColor = '#FF0000';
}

colorear();

function colorearColor(color){
    document.bgColor = color;
}

colorearColor('#00FF00');

function pideNumero(){
    var numero;
    do{
        numero = prompt('Dame un número', '');
        numero = Number(numero);
    }while(isNaN(numero));
    return numero;
}

/* En JS los parámetros se pasan por valor */

function referencia(x){
    x = 55;
}

var x=11;
referencia(x);
console.log(x);

function ambito(){
    var local=33;
    global=55;
}

ambito();
console.log(local);
console.log(global);

for(i in vector){
    /* repetirse tantas veces como posiciones tenga array vector */
}