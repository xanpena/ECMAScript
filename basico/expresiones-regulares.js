// Expresiones regulares

// /patrones/modificadores

var frase = "La tienda hoy está cerrada";
// ¿Cuántas letras "a" hay en esta frase?
console.log(frase.match(/a/g));
console.log(frase.match(/a/));

var chiquito = "Lorem fistrum te voy a borrar el cerito quietooor te voy a borrar el cerito benemeritaar. Amatomaa al ataquerl llevame al sircoo caballo blanco caballo negroorl te va a hasé pupitaa diodeno se calle ustée me cago en tus muelas. De la pradera al ataquerl condemor se calle ustée hasta luego Lucas la caidita por la gloria de mi madre pecador mamaar. Al ataquerl ese hombree diodeno me cago en tus muelas. Jarl qué dise usteer no puedor ese hombree apetecan no te digo trigo por no llamarte Rodrigor llevame al sircoo diodenoo diodeno.";
// Buscamos dos "oo" consecutivas
console.log(chiquito.match(/oo/g));
console.log(chiquito.match(/[a-z]*oo/g));