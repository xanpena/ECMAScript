//Valor
var cad = "hola";
var cadcopy = cad;

cad = "Hola Juan";
console.log(cad);
console.log(cadcopy);

// Referencia
var lista = ['azul', 'amarillo', 'rojo', 'verde'];
var listacopy = lista;

lista[0]='negro';
console.log(lista);
console.log(listacopy);

// Valor. slice()
var array1 = ['boli', 'lapiz', 'goma', 'regla'];
var array1copy = array1.slice();

lista[0]='escritorio';
console.log(array1);
console.log(array1copy);

// Referencia objetos
var obj={
    "test":2,
    "final":4
    }
console.log(typeof(obj));
var objcopy = obj;

obj.final = 8;
console.log(obj);
console.log(objcopy);