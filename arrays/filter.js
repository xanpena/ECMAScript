const words = ['one', 'limit', 'blockchain', 'market']

const newArray = []
for (let index = 0; index < words.length; index++) {
    const item = words[index]
    if (item.length >= 6) {
        newArray.push(item)
    }
}

console.log(words)
console.log(newArray)

const withFilter = words.filter(item => item.length >= 6)

console.log(withFilter)
