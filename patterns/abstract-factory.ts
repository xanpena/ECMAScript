
interface MastodonCar {
    useGPS(): void
}

interface RhinoCar {
    useGPS(): void
}

class MastodonSedanCar implements MastodonCar {
    useGPS(): void {
        console.log('[SEDAN] Mastodon GPS')
    }
}

class MastodonHatchbackCar implements MastodonCar {
    useGPS(): void {
        console.log('[HATCHBACK] Mastodon GPS')
    }
}

class RhinoSedanCar implements RhinoCar {
    useGPS(): void {
        console.log('[SEDAN] Rhino GPS')
    }
}

class RhinoHatchbackCar implements RhinoCar {
    useGPS(): void {
        console.log('[HATCHBACK] Rhino GPS')
    }
}

interface ClassAbstractFactory {
    createMastodon(): MastodonCar
    createRhino(): RhinoCar
}

class SedanCarFactory implements ClassAbstractFactory {
    createMastodon(): MastodonCar {
        return new MastodonSedanCar()
    }
    
    createRhino(): RhinoCar {
        return new RhinoSedanCar()
    }
}

class HatchbackCarFactory implements ClassAbstractFactory {
    createMastodon(): MastodonCar {
        return new MastodonHatchbackCar()
    }
    
    createRhino(): RhinoCar {
        return new RhinoHatchbackCar()
    }
}

function appCarFactory(factory: CarAbstractFactory) {
    const mastodon: MastodonCar = factory.createMastodon()
    const rhino: RhinoCar = factory.createRhino()

    mastodon.useGPS()
    rhino.useGPS()
}

appCarFactory(new HatchbackCarFactory())
appCarFactory(new SedanCarFactory())

type FactoryType = 'sedan' | 'hatchback'

function createFactory(type: FactoryType) : CarAbstractFactory {
    const factories = {
        hatchback: HatchbackCarFactory,
        sedan: SedanCarFactory,
    }

    const Factory = factories[type]
    return new Factory()
}

appCarFactory(createFactory('hatchback'))
appCarFactory(createFactory('sedan'))