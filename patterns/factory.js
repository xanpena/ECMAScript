
/**
 * "Abstract class"
 */
class BaseCar {
    showCost() {
        throw new Error('Method not implemented')
    }
}

/**
 * Models
 */
class MastodonCar extends BaseCar {
    showCost() {
        console.log('Mastodon car cost: $ 20.000')
    }
}

class RhinoCar extends BaseCar {
    showCost() {
        console.log('RhinoCar car cost: $ 15.000')
    }
}

/**
 * Factories
 */
class CarFactory {
    makeCar() {
        throw new Error('Method not implemented')
    }
}

class MastodonCarFactory extends CarFactory {
    makeCar() {
        return new MastodonCar()
    }
}

class RhinoCarFactory extends CarFactory {
    makeCar() {
        return new RhinoCar()
    }
}

function useFactoryPatter(factory) {
    const car = factory.makeCar()
    car.showCost()
}

useFactoryPatter(new MastodonCarFactory()) // Mastodon car cost: $ 20.000
useFactoryPatter(new RhinoCarFactory())    // RhinoCar car cost: $ 15.000

function createFactory(type) {
    const factories = {
        mastodon: MastodonCarFactory,
        rhino: RhinoCarFactory
    }

    const factory = factories[type]
    return new factory()
}

useFactoryPatter(createFactory('mastodon')) // Mastodon car cost: $ 20.000
useFactoryPatter(createFactory('rhino'))    // RhinoCar car cost: $ 15.000