
class Singleton {
    static instance = undefined

    constructor() {

    }

    static getInstance() {
        if (!Singleton.instance) {
            Singleton.instance = new Singleton()
        }

        return Singleton.instance
    }
}