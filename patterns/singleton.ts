
class SingletonTS {
    private static instance: SingletonTS

    private constructor() {

    }

    static getInstance() : SingletonTS {
        if (!SingletonTS.instance) {
            SingletonTS.instance = new SingletonTS()
        }

        return SingletonTS.instance
    }
}