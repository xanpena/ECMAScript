const puppeteer = require('puppeteer');

describe('Test browser', () => {
  it('Open and close browser', async () => {
    const browser = await puppeteer.launch({
      headless: false,
    });
    const page = await browser.newPage();
    await page.goto('https://www.google.com');
    await browser.close();
  }, 10000);
});
