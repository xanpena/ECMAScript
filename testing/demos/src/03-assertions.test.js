test('test object', () => {
  const data = { name: 'xan' };
  data.lastname = 'pena';
  expect(data).toEqual({ name: 'xan', lastname: 'pena' });
});

test('test null', () => {
  const data = null;
  expect(data).toBeNull();
  expect(data).toBeDefined();
  expect(data).not.toBeUndefined();
});

test('test booleans', () => {
  expect(true).toEqual(true);
  expect(false).toEqual(false);
  expect(0).toBeFalsy();
  expect('').toBeFalsy();
  expect(false).toBeFalsy();
});

test('test strings', () => {
  expect('Xan').toMatch(/X/);
});

test('test arays', () => {
  const numbers = [1, 2, 3, 4];
  expect(numbers).toContain(3);
});
