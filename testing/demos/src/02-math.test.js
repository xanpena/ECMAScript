const { sum, multiply, divide } = require('./02-math');

test('adds 1 + 2 to equal 3', () => {
  expect(sum(1, 2)).toBe(3);
});

test('multiply 1 * 2 to equal 2', () => {
  expect(multiply(1, 2)).toBe(2);
});

test('divide 4 / 2 to equal 2', () => {
  expect(divide(4, 2)).toBe(2);
});

test('divide by zero expects null', () => {
  expect(divide(4, 0)).toBeNull();
});
