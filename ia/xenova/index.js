import { pipeline } from "@xenova/transformers";
import wavefile from "wavefile";
import fs from 'fs'

const EMBED = 'https://huggingface.co/datasets/Xenova/transformers.js-docs/resolve/main/speaker_embeddings.bin'
const PHRASE = 'Are you really prepare to the next world?'

const synthetizer = await pipeline(
    'text-to-speech',
    'Xenova/speecht5_tts',
    { quantized: false}
)

const output = await synthetizer(
    PHRASE,
    { speaker_embeddings: EMBED}
)

const wav = new wavefile.WaveFile()
wav.fromScratch(1, output.sampling_rate, '32f', output.audio)
fs.writeFileSync('out.wav', wav.toBuffer());