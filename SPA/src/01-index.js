const app = document.getElementById('app')

const header = document.createElement('h1')

const text = 'SPA with vanilla JavaScript'
const headerContent = document.createTextNode(text)

header.appendChild(headerContent)

app.appendChild(header)