function Persona(nombre, apellido){
    this.nombre = nombre
    this.apellido = apellido
    return this
}

Persona.prototype.saludar = function(){
    console.log(`Hola, me llamo ${this.nombre}, ${this.apellido}`)
}

var persona = new Persona('Xan', 'Pena')
persona.saludar()