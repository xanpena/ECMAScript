// Callbacks, Promises

const operation = (number1, number2, op) => {
    return op(number1, number2)
}

operation(1, 3, (a, b) => a + b)
operation(1, 3, (a, b) => a - b)
operation(1, 3, (a, b) => a * b)



const operationWithCallback = (number, number2, callback) => {
    const result = number1 + number2
    return setTimeout(() => {
        callback(result)
    }, 500)
}

const operationWithPromise = (number1, number2) => {
    const result = number1 + number2
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(result)
        })
    })
}

operationWithCallback(1, 3, (result) => {
    console.log(result)
})

operationWithPromise(1, 3)
    .then(result => console.log(result))

// https://www.youtube.com/watch?v=frm0CHyeSbE&ab_channel=midulive